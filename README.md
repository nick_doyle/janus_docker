# Janus environment
Includes

- Janus (Gremlin server speaking Websockets on port 8182)
- Janus (Gremlin server speaking HTTP on port 8183)
- Cassandra (with RPC / thrift enabled on 9160)
- Elasticsearch (stock apart from mem)
- Linkurious (on alpine/glibc base image)

# Notes

## Janus
Janus/gremlin normally comes up too quickly and doesn't execute the groovy startup script which defines the default traversal() "g".

Symptoms include error like "no property 'g'"

To fix:

`docker restart janus-websocket janus-http`

We're actually running *two* Gremlin servers; one serving websockets and the other http.
Apparently one server can't do both, yet.
The main reason is easier data loading via curl to http, whereas linkurious *needs* to use websockets (http doesn't work)

## Linkurious Config

### Graph Server

Uses the containers as defined in dockerfile.
They're accessible by their container names from other containers (e.g. the linkurious one) because they're within the same docker network

Name                    | Value
------------------------|----------------------------------------
Vendor                  | JanusGraph
URL                     | ws://janus-websocket:8182
Configuration object    | {
                        |  "storage.backend": "cassandra",
                        |  "storage.hostname": "cassandra-server",
                        |  "index.search.backend": "elasticsearch",
                        |  "index.search.hostname": "es-server"
                        | }

### Search Index Server

This will use linkurious' builtin ES server
We do this because it doesn't support newer versions of ES

Name                    | Value
------------------------|----------------------------------------
Vendor                  | Elasticsearch
Host                    | 127.0.0.1
Port                    | 9201
